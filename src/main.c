/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32l1xx.h"
#include "stm32l1xx_nucleo.h"
#include <cmsis_os.h>
#include <stdbool.h>
			
void blinker_task(const void *args) {
    while(true) {
        // change led state
        BSP_LED_Toggle(LED_GREEN);
        osDelay(100);
    }
}
osThreadDef (Blinker, blinker_task, osPriorityNormal, 1, configMINIMAL_STACK_SIZE);


int main(void)
{
    BSP_LED_Init(LED_GREEN);

    osThreadCreate(osThread(Blinker), NULL);

    osKernelStart();

	for(;;) {

	}
}
